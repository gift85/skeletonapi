
docker-compose up -d

docker exec composer install 

docker exec php bin/console doctrine:migrations:migrate

docker exec php bin/console doctrine:fixtures:load