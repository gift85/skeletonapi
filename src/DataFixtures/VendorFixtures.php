<?php

namespace App\DataFixtures;

use App\Entity\Vendor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class VendorFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $vendors = ['Bosch', 'Makita', 'Black&Decker', 'Hitachi', 'DeWalt'];

        for ($i = 0; $i < 10; $i++) {
            $vendor = new Vendor();
            $vendor->setTitle($vendors[mt_rand(0, count($vendors) - 1)]);
            $manager->persist($vendor);
        }

        $manager->flush();
    }
}
