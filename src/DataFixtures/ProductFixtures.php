<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\Vendor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $first = ['Лента', 'Шкурка', 'Круг', 'Тигель', 'Диск'];
        $second = ['шлифовальная', 'электрический', 'пильный', ''];
        $third = ['бесконечная', 'в рулоне', 'твердосплавный', ''];
        $vendorRepository = $manager->getRepository(Vendor::class);

        for ($i = 0; $i < 20; $i++) {
            $firstIndex = mt_rand(0, count($first) - 1);
            $secondIndex = mt_rand(0, count($second) - 1);
            $thirdIndex = mt_rand(0, count($third) - 1);
            $title = [$first[$firstIndex], $second[$secondIndex], $third[$thirdIndex]];

            $product = new Product();
            $product->setTitle(implode(' ', $title));
            $product->setVendor($vendorRepository->findOneRandom());
            $product->setPrice(mt_rand(9, 2100));
            $manager->persist($product);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            VendorFixtures::class,
        ];
    }
}
