<?php

namespace App\Controller\Rest;

use App\Entity\Product;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Repository\ProductRepository;

class ProductsController extends FOSRestController
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @Rest\Get("/products")
     */
    public function getProducts(): View
    {
        $products = $this->productRepository->findAll();
        return View::create($products, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/products/{id}")
     * @param int $id product id
     * @return View
     */
    public function getProduct(int $id): View
    {
        $product = $this->productRepository->find($id);
        return View::create($product, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/products")
     * @param Request $request
     * @return View
     */
    public function postProduct(Request $request): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = new Product();
        $product->setTitle($request->get('title'));
        $product->setVendor($request->get('vendor'));
        $product->setPrice($request->get('price'));
        $entityManager->persist($product);
        $entityManager->flush();
        return View::create($product, Response::HTTP_CREATED);
    }

    /**
     * @Rest\Delete("/products/{id}")
     * @param int $id
     * @return View
     */
    public function deleteProduct(int $id): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $this->productRepository->find($id);

        if ($product) {
            $entityManager->remove($product);
            $entityManager->flush();
        }

        return View::create([], Response::HTTP_NO_CONTENT);
    }
}
