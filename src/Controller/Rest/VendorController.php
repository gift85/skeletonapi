<?php

namespace App\Controller\Rest;

use App\Entity\Product;
use App\Entity\Vendor;
use App\Repository\VendorRepository;
use Doctrine\Common\Collections\Criteria;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

class VendorController extends FOSRestController
{
    private $vendorRepository;

    public function __construct(VendorRepository $vendorRepository)
    {
        $this->vendorRepository = $vendorRepository;
    }

    /**
     * @Rest\Get("/vendors/")
     * @param Request $request
     * @return View
     */
    public function getVendors(Request $request): View
    {
        /* not working */
//        if ($request->query->has('hasMoreThan')) {
//            $criteria = Criteria::create()
//                ->where(Criteria::expr()->qt('products', $request->query->get('hasMoreThan')));
//            $vendors = $this->vendorRepository->matching($criteria);
//            $vendors = $this->vendorRepository->($criteria);
//            $vendors = $this->vendorRepository->findHasMoreThan($request->query->get('hasMoreThan'));
//        } else {
            $vendors = $this->vendorRepository->findAll();
//        }

        return View::create($vendors, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/vendors/{id}")
     * @param int $id vendor id
     * @return View
     */
    public function getVendor(int $id): View
    {
        $vendor = $this->vendorRepository->find($id);
        return View::create($vendor, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/vendors/{id}/products")
     * @param int $id vendor id
     * @return View
     */
    public function getVendorProducts(int $id): View
    {
        $productRepository = $this->getDoctrine()->getManager()->getRepository(Product::class);
        $product = $productRepository->findBy(['vendor' => $id]);
        return View::create($product, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/vendors")
     * @param Request $request
     * @return View
     */
    public function postVendor(Request $request): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $vendor = new Vendor();
        $vendor->setTitle($request->get('title'));
        $entityManager->persist($vendor);
        $entityManager->flush();
        return View::create($vendor, Response::HTTP_CREATED);
    }

    /**
     * @Rest\Delete("/vendors/{id}")
     * @param int $id
     * @return View
     */
    public function deleteVendor(int $id): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $vendor = $this->vendorRepository->find($id);

        if ($vendor) {
            $entityManager->remove($vendor);
            $entityManager->flush();
        }

        return View::create([], Response::HTTP_NO_CONTENT);
    }
}
