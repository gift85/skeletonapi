<?php

namespace App\Models;


use Symfony\Component\Cache\Simple\FilesystemCache;

class Currency
{
    private $url = 'https://www.cbr-xml-daily.ru/daily_json.js';
    private $daily;
    private $baseCurrency = 'RUB';
    private $currency;

    public function __construct($currency)
    {
        $this->currency = $currency;
        $cache = new FilesystemCache();

        if ($cache->has('currencies')) {
            $this->daily = $cache->get('currencies');
        } else {
            $this->daily = new \SimpleXMLElement($this->url);
            $cache->set('currencies', $this->daily, new \DateTime('tomorrow'));
        }
    }


}