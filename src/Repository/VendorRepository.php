<?php

namespace App\Repository;

use App\Entity\Vendor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Vendor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vendor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vendor[]    findAll()
 * @method Vendor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VendorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Vendor::class);
    }

    public function findOneRandom()
    {
        $id_limits = $this->createQueryBuilder('vendor')
            ->select('MIN(vendor.id)', 'MAX(vendor.id)')
            ->getQuery()
            ->getOneOrNullResult();
        $random_possible_id = rand($id_limits[1], $id_limits[2]);

        return $this->createQueryBuilder('vendor')
            ->where('vendor.id >= :random_id')
            ->setParameter('random_id', $random_possible_id)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /* not working */
    public function findHasMoreThan(int $int)
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT v, count(p.id) as pr
        FROM App\Entity\Vendor v
        OUTER JOIN v.products p
        WHERE pr > 1
        GROUP BY v.id'
        )->setParameter('int', $int);
        return $query->execute();
        return $this->createQueryBuilder('v')
            ->select('v.id', 'v.title', 'count(p.id) as products')
            ->leftJoin('product', 'p')
            ->where('products > 1')
            ->setParameter('int', $int)
            ->groupBy('v.id')
            ->getQuery()
            ->getArrayResult();
    }
}
